package com.freenow.android_demo.activities.idling;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

import com.freenow.android_demo.activities.AuthenticatedActivity;

abstract public class Idling extends AuthenticatedActivity {
    // The Idling Resource which will be null in production.
    @Nullable
    protected static SimpleIdlingResource mIdlingResource;

    @VisibleForTesting
    @NonNull
    public static SimpleIdlingResource getIdlingResource() {
        if (mIdlingResource == null) {
            mIdlingResource = new SimpleIdlingResource();
        }
        return mIdlingResource;
    }
}
