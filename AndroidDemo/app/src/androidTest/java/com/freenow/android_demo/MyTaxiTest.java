package com.freenow.android_demo;


import android.app.Activity;
import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.GrantPermissionRule;
import android.support.test.runner.AndroidJUnit4;

import com.freenow.android_demo.activities.MainActivity;
import com.freenow.android_demo.activities.idling.SimpleIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MyTaxiTest {

    private static final String LOGIN_USER = "crazydog335";
    private static final String LOGIN_PASSWORD = "venture";
    private static final Uri INTENT_PHONE_NUMBER = Uri.parse("tel:413-868-2228");
    private static final String DRIVER_NAME = "Sarah Scott";
    private static final String SEARCH = DRIVER_NAME.substring(0,2).toLowerCase();

    private SimpleIdlingResource mIdlingResource;

    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.ACCESS_FINE_LOCATION);

    @Rule
    public IntentsTestRule<MainActivity> activityTestRule =
            new IntentsTestRule<>(MainActivity.class, false, false);


    @Before
    public void removeResourceAndLaunchActivity() {
        File root = InstrumentationRegistry.getTargetContext().getFilesDir().getParentFile();
        String[] sharedPreferencesFileNames = new File(root, "shared_prefs").list();
        if(sharedPreferencesFileNames != null){
            for (String fileName : sharedPreferencesFileNames) {
                InstrumentationRegistry.getTargetContext().getSharedPreferences(fileName.replace(".xml", ""), Context.MODE_PRIVATE).edit().clear().commit();
            }
        }

        activityTestRule.launchActivity(null);
        intending(not(isInternal())).respondWith(new Instrumentation.ActivityResult(Activity.RESULT_OK, null));
        mIdlingResource = activityTestRule.getActivity().getIdlingResource();
        // To prove that the test fails, omit this call:
        Espresso.registerIdlingResources(mIdlingResource);
    }


    @Test
    public void selectDriverTest() {

        onView(withId(R.id.edt_username)).perform(typeText(LOGIN_USER), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(LOGIN_PASSWORD), closeSoftKeyboard());
        onView(withId(R.id.btn_login)).perform(click());

        for (int i = 0; i < SEARCH.length(); i++) {
            onView(withId(R.id.textSearch)).perform(typeText(Character.toString(SEARCH.charAt(i))));
        }

        onView(withText(DRIVER_NAME)).inRoot(RootMatchers.isPlatformPopup()).perform(click());

        onView(withId(R.id.textViewDriverName)).check(matches(withText(DRIVER_NAME)));
        onView(withId(R.id.fab)).perform(click());

        intended(allOf(
                hasAction(Intent.ACTION_DIAL),
                hasData(INTENT_PHONE_NUMBER)));
    }

    @After
    public void unregisterIdlingResource() {
        Espresso.unregisterIdlingResources(mIdlingResource);
    }
}
